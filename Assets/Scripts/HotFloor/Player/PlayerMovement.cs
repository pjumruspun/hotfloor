﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField]
        private LayerMask groundLayer;

        [SerializeField]
        private float holdingJumpTimeLimit = 0.25f;

        [SerializeField]
        private float jumpForce = 7.0f;

        private float verticalVelocity;
        private float playerMovementSpeed = 4.0f;
        private Rigidbody2D rb;
        private float jumpTimeCounter = 1.0f;
        
        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        private void Update()
        {
            ProcessJumping();
            ProcessMovement();

        }

        private void ProcessMovement()
        {
            Vector2 moveVector = Vector2.zero;
            moveVector.x = playerMovementSpeed;
            rb.velocity = new Vector2(moveVector.x, rb.velocity.y);
        }
        private void ProcessJumping()
        {
            if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
            {
                rb.velocity = new Vector2(0.0f, jumpForce);
                jumpTimeCounter = 0.0f;

            }

            if (Input.GetKey(KeyCode.Space) && jumpTimeCounter < holdingJumpTimeLimit)
            {
                rb.velocity = new Vector2(0.0f, jumpForce);
                jumpTimeCounter += Time.deltaTime;

            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                jumpTimeCounter = holdingJumpTimeLimit + 1.0f;
            }
        }

        bool IsGrounded()
        {
            Vector2 position = transform.position;
            Vector2 direction = Vector2.down;
            float distance = 0.6f;
            Debug.DrawRay(position, direction * distance, Color.green);
            RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, groundLayer);

            if (hit.collider != null)
            {
                return true;
            }

            return false;
        }
    }
}