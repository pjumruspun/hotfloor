﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Environment
{
    public class PlatformController : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] platforms;

        private bool platform0IsActive = true;
        private Transform[] triggers;
        private Vector3 nextPlatformOffset = Vector3.right * 34.5f;
        // Start is called before the first frame update

        public void SwapPlatform()
        {
            if (platform0IsActive)
            {
                // Swap platforms[1] in
                platforms[1].transform.position = platforms[0].transform.position + nextPlatformOffset;
                platform0IsActive = false;
            }

            else
            {
                // Swap platforms[0] in
                platforms[0].transform.position = platforms[1].transform.position + nextPlatformOffset;
                platform0IsActive = true;
            }
        }
        void Start()
        {
            if (platforms.Length == 0)
            {
                Debug.Assert(false, "[ERROR] PlatformController : Please add 2 platforms into the array");
            }

            triggers = new Transform[]
            {
                platforms[0].transform.Find("PlatformTrigger"),
                platforms[1].transform.Find("PlatformTrigger"),
            };
        }
    }
}