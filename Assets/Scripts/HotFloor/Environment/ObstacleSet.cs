﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Environment
{
    [System.Serializable]
    public class ObstacleSet
    {
        public Obstacle[] Obstacles => obstacles;
        [SerializeField]
        private Obstacle[] obstacles;
    }
}