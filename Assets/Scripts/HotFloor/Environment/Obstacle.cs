﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Environment
{
    [System.Serializable]
    public class Obstacle
    {
        [SerializeField]
        private Type obstacleType;

        [SerializeField]
        private int distanceFromLastObstacleSet;

        public Type ObstacleType => obstacleType;
        public int DistanceFromLastObstacleSet => distanceFromLastObstacleSet;
        public enum Type
        {
            Static,
            Moving,
            CeilingSpike
        }
    }
}