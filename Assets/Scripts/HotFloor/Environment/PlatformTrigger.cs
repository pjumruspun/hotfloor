﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Environment
{
    public class PlatformTrigger : MonoBehaviour
    {
        private PlatformController platformController;
        void Start()
        {
            platformController = GameObject.FindGameObjectWithTag("PlatformController").GetComponent<PlatformController>();
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag == "Player")
            {
                // Swap platform places
                platformController.SwapPlatform();
            }
        }
    }
}