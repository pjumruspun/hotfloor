﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Environment
{
    public class ObstacleGenerator : MonoBehaviour
    {
        [SerializeField]
        private GameObject staticObstaclePrefab;

        [SerializeField]
        private GameObject movingObstaclePrefab;

        [SerializeField]
        private GameObject spikeObstaclePrefab;

        [SerializeField]
        private ObstacleSet[] AllEasyObstacleSets;

        [SerializeField]
        private ObstacleSet[] AllHardObstacleSets;

        [SerializeField]
        private int startingDistance = 10;

        private const int staticObstaclePoolSize = 30;
        private const float staticObstacleYOffset = -0.5f;
        private GameObject player;
        private const int distanceToRenderObstacle = 3000; // Instantiate ObstacleSet up to 3000 pixels ahead
        private int distanceRendered = 0;
        private int currentDistance = 0;
        private int currentStaticObstacleNumber = 0;
        private Obstacle[] obstaclesToSpawn;
        private GameObject[] staticObstaclePool;

        void Start()
        {
            distanceRendered += 1000;
            player = GameObject.FindGameObjectWithTag("Player");
            staticObstaclePool = new GameObject[staticObstaclePoolSize];
            for(int i = 0; i < staticObstaclePoolSize; ++i)
            {
                GameObject instantiatedObj = Instantiate(staticObstaclePrefab);
                instantiatedObj.SetActive(false);
                staticObstaclePool[i] = instantiatedObj;
            }
        }
        void Update()
        {
            currentDistance = (int) player.transform.position.x * 100;
            while(distanceRendered < currentDistance + distanceToRenderObstacle)
            {
                int random = Random.Range(0, AllEasyObstacleSets.Length);
                SpawnObstacleSet(AllEasyObstacleSets[random]);
            }
        }

        void SpawnObstacleSet(ObstacleSet obstacleSet)
        {
            Debug.DrawRay(new Vector3(distanceRendered/100, 1.0f, 0.0f), Vector3.down, Color.green, 5.0f);
            for(int i = 0; i < obstacleSet.Obstacles.Length; ++i)
            {
                Obstacle obstacle = obstacleSet.Obstacles[i];
                switch(obstacle.ObstacleType)
                {
                    case Obstacle.Type.Static:
                        GameObject objectToSpawn = staticObstaclePool[currentStaticObstacleNumber % staticObstaclePoolSize];
                        objectToSpawn.SetActive(true);
                        objectToSpawn.transform.position = new Vector3((distanceRendered + obstacle.DistanceFromLastObstacleSet) / 100, -0.5f, 0.0f);
                        ++currentStaticObstacleNumber;
                        break;
                }
            }
            distanceRendered += obstacleSet.Obstacles[obstacleSet.Obstacles.Length - 1].DistanceFromLastObstacleSet;
            Debug.Log("Distance rendered = " + distanceRendered);
        }
    }
}