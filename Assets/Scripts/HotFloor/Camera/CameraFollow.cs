﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.HotFloor.Camera
{
    public class CameraFollow : MonoBehaviour
    {
        private Vector3 offset = new Vector3(1.75f, 0.0f, -10.0f);
        private Transform player;
        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            transform.position = player.position.x * Vector3.right + offset;
        }
    }
}