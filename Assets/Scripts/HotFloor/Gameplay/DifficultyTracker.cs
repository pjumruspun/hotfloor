﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyTracker : MonoBehaviour
{
    public enum Difficulty
    {
        Easy,
        Hard
    }

    private Difficulty difficulty = Difficulty.Easy;
    private float time = 0.0f;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if(time > 10.0f && difficulty == Difficulty.Easy)
        {
            difficulty = Difficulty.Hard;
            Debug.Log("Hard now!");
        }
    }
}
